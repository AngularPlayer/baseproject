function importAll (r) {
  r.keys().forEach(r);
}

importAll(require.context('.', true, /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/));
importAll(require.context('.', true, /\.js$/));
importAll(require.context('.', true, /\.html$/));
require('./core.scss');