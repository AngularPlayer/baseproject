const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const writeFileWebpackPlugin = require('write-file-webpack-plugin');

const WriteFileDevServe = new writeFileWebpackPlugin();
const cleanDist = new CleanWebpackPlugin(['dist']);

const extractHTML = new HtmlWebpackPlugin({ 
  title:'Custom template',
  template: 'src/index.html',
  alwaysWriteToDisk: true
});


const extractScssPlugin = new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "css/bundle.css",
      //filename: "[name].[hash].css",
      chunkFilename: "[id].css"
      //chunkFilename: "[id].[hash].css"
});

const jsRule = {
	test: /\.js$/,
    use: 'babel-loader'
}

const scssRule = {
    test: /\.scss$/,
    use: [

       MiniCssExtractPlugin.loader,
      {
        loader : 'css-loader', 
          options : {'minimize':true}
        },
    // { loader: 'namespace-css-module-loader'},
      'sass-loader',
      'import-glob-loader',
    ]
}


const htmlRule = {
  test: /[^index]\.html$/,
  use: [
    {
      loader:'file-loader',
      options: {
        name: '[name].html',
        outputPath:'templates'
      }
    },
    {loader:'extract-loader'},
    {
      loader: 'html-loader',
      options: {
        attrs: [":src",":ng-src"],
        interpolate: false,
        removeAttributeQuotes:false,
        minimize: true,
        removeComments: true,
        collapseWhitespace: false
      }
    }
  ]
}

const assetRule = {
  test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/,
  loader: 'file-loader',
  options: {
    // name: '[path][name].[ext]',
    name: '[name].[ext]',
    // publicPath: 'http://exemple.com/assets/',
    publicPath: 'assets/',
    outputPath:'assets',
    context: 'src/'
  }
}

module.exports = {
	entry: './index.js',
	output: {
	path: __dirname + '/dist',
		filename: 'bundle.js'
	},
	plugins: [
    cleanDist,
		extractHTML,
		extractScssPlugin,
    WriteFileDevServe
	],
	module: {
        rules: [
        	//jsRule,
      		scssRule,
      		htmlRule,
          assetRule
    	]
    }
}